package hepl.sysdist.item;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Item {
    @Id
    @GeneratedValue
    private Long itemId;

    private String nameItem;

    private Long providerId;

    private Long categoryId;

    private Double amountVat;

    public Long getItemId() {
        return itemId;
    }

    public String getNameItem() {
        return nameItem;
    }

    public Long getProviderId() {
        return providerId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Double getAmountVat() {
        return amountVat;
    }
}
