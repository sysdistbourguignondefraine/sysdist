package hepl.sysdist.order;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class OrderController {
    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/clientorder")
    Order createOrder(@RequestBody OrderDto orderDto) {
        List<Item> items = convertToEntry(orderDto);
        return orderService.createOrder(orderDto.getClientId(), orderDto.getStatus(), orderDto.getAmount(), items);
    }

    private List<Item> convertToEntry(OrderDto orderDto) {
        List<Item> items = new LinkedList<>();
        for (ItemDto it : orderDto.getItems()) {
            items.add(new Item(it.getItemId(), it.getQuantity()));
        }
        return items;
    }

}
