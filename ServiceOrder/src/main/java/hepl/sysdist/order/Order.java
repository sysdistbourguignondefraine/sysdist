package hepl.sysdist.order;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ClientOrder")
public class Order {
    @Id
    @GeneratedValue
    private Long orderId;

    private Long clientId;

    private String status;

    private double amount;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Item> items;

    public Order() {
    }

    public Order(Long clientId, String status, double amount, List<Item> items) {
        this.clientId = clientId;
        this.status = status;
        this.amount = amount;
        this.items = items;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getClientId() {
        return clientId;
    }

    public String getStatus() {
        return status;
    }

    public double getAmount() {
        return amount;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
