package hepl.sysdist.order;

public class ItemDto {
    private Long itemId;

    private int quantity;

    public Long getItemId() {
        return itemId;
    }

    public int getQuantity() {
        return quantity;
    }
}
