package hepl.sysdist.order;

import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;

@Entity
public class Item {
    @Id
    private Long itemId;

    private int quantity;

    public Item() {
    }

    public Item(Long itemId, int quantity) {
        this.itemId = itemId;
        this.quantity = quantity;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public Long getItemId() {
        return itemId;
    }
}
