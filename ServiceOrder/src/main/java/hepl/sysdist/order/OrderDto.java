package hepl.sysdist.order;

import java.util.List;

public class OrderDto {
    private Long clientId;

    private String status;

    private double amount;

    private List<ItemDto> items;

    public Long getClientId() {
        return clientId;
    }

    public String getStatus() {
        return status;
    }

    public double getAmount() {
        return amount;
    }

    public List<ItemDto> getItems() {
        return items;
    }
}
