#!/bin/bash
mvn clean package -f ServiceTVA
mvn clean package -f ServiceOrder
mvn clean package -f ServiceStock
mvn clean package -f ServiceCart
mvn clean package -f ServiceCheckout
mvn clean package -f ZuulGateway
mvn clean package -f ServeurEureka
mvn clean package -f ServiceFront
mvn clean package -f ServiceItem