package hepl.sysdist.stock;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InventoryService {
    private InventoryRepository inventoryRepository;

    public InventoryService(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    public boolean checkInventory(Long itemId, int quantity) {
        Optional<Stock> stock = inventoryRepository.findById(itemId);

        return stock.isPresent() && stock.get().getQuantity() >= quantity;
    }


}
