package hepl.sysdist.stock;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Stock {
    @Id
    private Long itemId;

    private int quantity;

    public int getQuantity() {
        return quantity;
    }
}
