package hepl.sysdist.checkout;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckoutController {
    private CheckoutService checkoutService;

    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @GetMapping("/checkout/{clientId}/{orderId}/{shippingMethod}")
    public Checkout doCheckout(@PathVariable("clientId") Long clientId, @PathVariable("orderId") Long orderId, @PathVariable("shippingMethod") String shippingMethod) {
        return checkoutService.doCheckout(clientId, orderId, shippingMethod);
    }
}
