package hepl.sysdist.checkout;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Checkout {
    @Id
    private Long orderId;

    private Long clientId;

    public Checkout() {
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getClientId() {
        return clientId;
    }
}
