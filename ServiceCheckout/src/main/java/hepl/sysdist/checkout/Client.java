package hepl.sysdist.checkout;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Client {
    @Id
    private Long clientId;

    private String nameUser;

    private String address;

    private double amountAvailable;

    public Client() {
    }

    public Long getClientId() {
        return clientId;
    }

    public String getNameUser() { return nameUser; }

    public String getAddress() {
        return address;
    }

    public double getAmountAvailable() {
        return amountAvailable;
    }
}
