package hepl.sysdist.cart.service;

import hepl.sysdist.cart.entity.Cart;
import hepl.sysdist.cart.entity.Entry;
import hepl.sysdist.cart.repository.CartRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartService {
    private CartRepository cartRepository;

    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public Cart getCart(Long clientId) {
        return cartRepository.findById(clientId).orElse(new Cart(clientId));
    }

    public Cart updateCart(Long clientId, Long itemId, int quantity) {
        Cart cart = getCart(clientId);

        Optional<Entry> existingEntry = cart
                .getEntries()
                .stream()
                .filter(e -> e.getItemId() == itemId)
                .findFirst();

        if (existingEntry.isPresent()) {
            Entry entry = existingEntry.get();
            entry.setQuantity(entry.getQuantity() + quantity);
            if (entry.getQuantity() <= 0) {
                cart.removeEntry(entry);
            }
        } else if (quantity > 0) {
            Entry entry = new Entry(itemId, quantity);
            cart.addEntry(entry);
        }

        return cartRepository.save(cart);
    }
}
