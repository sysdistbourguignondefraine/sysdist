package hepl.sysdist.cart.controller;

import hepl.sysdist.cart.dto.EntryDto;
import hepl.sysdist.cart.entity.Cart;
import hepl.sysdist.cart.service.CartService;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartController {
    private CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/cart/{clientId}")
    Cart getCart(@PathVariable("clientId") Long clientId) {
        return cartService.getCart(clientId);
    }

    @PostMapping("/cart/{clientId}")
    Cart addItem(@PathVariable("clientId") Long clientId, @RequestBody EntryDto entryDto) {
        return cartService.updateCart(clientId, entryDto.getItemId(), entryDto.getQuantity());
    }
}
