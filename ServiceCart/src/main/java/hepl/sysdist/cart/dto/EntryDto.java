package hepl.sysdist.cart.dto;

public class EntryDto {
    private Long itemId;
    private int quantity;

    public Long getItemId() {
        return itemId;
    }

    public int getQuantity() {
        return quantity;
    }
}
