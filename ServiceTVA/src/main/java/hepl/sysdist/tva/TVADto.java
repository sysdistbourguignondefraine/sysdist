package hepl.sysdist.tva;

public class TVADto {
    private Long categoryId;
    private int taux;

    public TVADto(Long categoryId, int taux) {
        this.categoryId = categoryId;
        this.taux = taux;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public int getTaux() {
        return taux;
    }
}
