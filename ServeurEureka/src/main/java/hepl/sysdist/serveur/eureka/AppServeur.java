package hepl.sysdist.serveur.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class AppServeur {
    public static void main(String[] args) {
        SpringApplication.run(AppServeur.class, args);
    }
}
