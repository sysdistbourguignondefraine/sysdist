package hepl.sysdist.front;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<Client, Integer> {
    Client getByUsername(String username);
}
