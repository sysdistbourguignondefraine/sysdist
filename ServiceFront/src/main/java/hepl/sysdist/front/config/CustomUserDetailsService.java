package hepl.sysdist.front.config;

import hepl.sysdist.front.Client;
import hepl.sysdist.front.UserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = userRepository.getByUsername(username);

        if (client == null) {
            throw new UsernameNotFoundException("Could not find user");
        }

        return new User(client.getUsername(), client.getPassword(), true, true, true, true, Collections.emptyList());
    }

}
