package hepl.sysdist.front.controller;

import hepl.sysdist.front.serviceclient.ItemServiceClient;
import hepl.sysdist.front.serviceclient.TVAServiceClient;
import hepl.sysdist.front.serviceclient.dto.ItemDto;
import hepl.sysdist.front.serviceclient.dto.TVADto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class HomeController {
    private ItemServiceClient itemServiceClient;
    private TVAServiceClient tvaServiceClient;


    public HomeController(ItemServiceClient itemServiceClient, TVAServiceClient tvaServiceClient) {
        this.itemServiceClient = itemServiceClient;
        this.tvaServiceClient = tvaServiceClient;
    }

    @GetMapping
    public String getHome(Model model, Authentication authentication) {

        List<ItemDto> items = itemServiceClient.getItems();

        Map<Long, Double> taux = new LinkedHashMap<>();
        int i = 0;
        for(ItemDto it : items) {
            taux.put(it.getCategoryId(), (double) tvaServiceClient.getTVA((long) i).getTaux());
            i++;
        }

        model.addAttribute("items", items);
        model.addAttribute("taux", taux);

        if (authentication != null && authentication.isAuthenticated()) {
            User principal = (User) authentication.getPrincipal();

            model.addAttribute("authenticated", true);
            model.addAttribute("username", principal.getUsername());
        } else {
            model.addAttribute("authenticated", false);
        }

        return "home";
    }
}
