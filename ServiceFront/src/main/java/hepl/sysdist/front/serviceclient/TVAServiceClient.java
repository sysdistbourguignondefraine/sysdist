package hepl.sysdist.front.serviceclient;

import hepl.sysdist.front.serviceclient.dto.TVADto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("SERVICE-TVA")
public interface TVAServiceClient {
    @RequestMapping(method = RequestMethod.GET, value = "/tva/{categoryId}", consumes = "application/json")
    TVADto getTVA(@PathVariable("categoryId") Long categoryId);
}
