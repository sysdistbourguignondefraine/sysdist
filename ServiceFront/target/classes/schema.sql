DROP TABLE IF EXISTS CLIENT;
CREATE TABLE Client (
     `client_id` Long NOT NULL,
     `username` varchar(45) DEFAULT NULL,
     `password` varchar(64) DEFAULT NULL,
     `role` varchar(45) DEFAULT NULL,
     `enabled` tinyint DEFAULT NULL
);

